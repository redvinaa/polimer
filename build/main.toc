\contentsline {chapter}{\numberline {1}El\IeC {\H o}ad\IeC {\'a}sok}{1}{chapter.1}
\contentsline {section}{\numberline {1.2}M\IeC {\'a}sodik el\IeC {\H o}ad\IeC {\'a}s}{1}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Sz\IeC {\'e}n}{1}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}K\IeC {\'e}miai k\IeC {\"o}t\IeC {\'e}sek}{1}{subsection.1.2.2}
\contentsline {subsection}{\numberline {1.2.3}M\IeC {\'a}sodrend\IeC {\H u} k\IeC {\"o}t\IeC {\'e}sek}{2}{subsection.1.2.3}
\contentsline {section}{\numberline {1.3}Harmadik el\IeC {\H o}ad\IeC {\'a}s}{2}{section.1.3}
\contentsline {subsection}{\numberline {1.3.1}Polimerek el\IeC {\H o}\IeC {\'a}ll\IeC {\'\i }t\IeC {\'a}sa}{2}{subsection.1.3.1}
\contentsline {subsubsection}{\numberline {1.3.1.1}L\IeC {\'e}pcs\IeC {\H o}s}{2}{subsubsection.1.3.1.1}
\contentsline {subsubsection}{\numberline {1.3.1.2}Folyamatos}{3}{subsubsection.1.3.1.2}
\contentsline {subsection}{\numberline {1.3.2}Polimeriz\IeC {\'a}ci\IeC {\'o}s reaktort\IeC {\'\i }pusok}{3}{subsection.1.3.2}
\contentsline {subsection}{\numberline {1.3.3}Polimerek f\IeC {\H o}bb jellemz\IeC {\H o}i}{4}{subsection.1.3.3}
\contentsline {section}{\numberline {1.4}Negyedik el\IeC {\H o}ad\IeC {\'a}s}{5}{section.1.4}
\contentsline {subsubsection}{\numberline {1.4.0.1}Kopolimerek csoportos\IeC {\'\i }t\IeC {\'a}sa}{6}{subsubsection.1.4.0.1}
\contentsline {subsection}{\numberline {1.4.1}Molekul\IeC {\'a}k alakjai}{6}{subsection.1.4.1}
\contentsline {subsection}{\numberline {1.4.2}F\IeC {\'a}zisok}{6}{subsection.1.4.2}
\contentsline {section}{\numberline {1.5}\IeC {\"O}t\IeC {\"o}dik el\IeC {\H o}ad\IeC {\'a}s}{7}{section.1.5}
\contentsline {subsection}{\numberline {1.5.1}Polimerek fizikai \IeC {\'a}llapotai}{8}{subsection.1.5.1}
\contentsline {section}{\numberline {1.6}Hatodik el\IeC {\H o}ad\IeC {\'a}s}{11}{section.1.6}
\contentsline {subsection}{\numberline {1.6.1}Polimerek termodinamikai vizsg\IeC {\'a}lata\hspace {0.3cm}(\ref {sect:viszkoelaszticitas}. fejezet)}{11}{subsection.1.6.1}
\contentsline {subsection}{\numberline {1.6.2}Terhel\IeC {\'e}sek, vizsg\IeC {\'a}latok}{11}{subsection.1.6.2}
\contentsline {section}{\numberline {1.7}Hetedik el\IeC {\H o}ad\IeC {\'a}s}{13}{section.1.7}
\contentsline {subsection}{\numberline {1.7.1}K\IeC {\'u}sz\IeC {\'a}s}{13}{subsection.1.7.1}
\contentsline {subsubsection}{\numberline {1.7.1.1}Burgers modell}{14}{subsubsection.1.7.1.1}
\contentsline {subsubsection}{\numberline {1.7.1.2}Stuart modell}{15}{subsubsection.1.7.1.2}
\contentsline {subsection}{\numberline {1.7.2}M\IeC {\'e}retez\IeC {\'e}s}{15}{subsection.1.7.2}
\contentsline {subsection}{\numberline {1.7.3}Fesz\IeC {\"u}lts\IeC {\'e}g relax\IeC {\'a}ci\IeC {\'o}}{15}{subsection.1.7.3}
\contentsline {subsubsection}{\numberline {1.7.3.1}Maxwell modell}{15}{subsubsection.1.7.3.1}
\contentsline {subsubsection}{\numberline {1.7.3.2}Standard-Solid modell}{16}{subsubsection.1.7.3.2}
\contentsline {subsection}{\numberline {1.7.4}\IeC {\"U}t\IeC {\H o}vizsg\IeC {\'a}latok}{16}{subsection.1.7.4}
\contentsline {section}{\numberline {1.8}Nyolcadik el\IeC {\H o}ad\IeC {\'a}s}{17}{section.1.8}
\contentsline {subsection}{\numberline {1.8.1}Newtoni folyad\IeC {\'e}kok}{18}{subsection.1.8.1}
\contentsline {subsection}{\numberline {1.8.2}Hatv\IeC {\'a}nyt\IeC {\"o}rv\IeC {\'e}ny (Oswalt - de Waele)}{18}{subsection.1.8.2}
\contentsline {subsection}{\numberline {1.8.3}Bingham}{19}{subsection.1.8.3}
\contentsline {subsection}{\numberline {1.8.4}Fluktu\IeC {\'a}ci\IeC {\'o}s h\IeC {\'a}l\IeC {\'o} modell}{19}{subsection.1.8.4}
\contentsline {subsection}{\numberline {1.8.5}Val\IeC {\'o}s polimer \IeC {\"o}mled\IeC {\'e}k}{19}{subsection.1.8.5}
\contentsline {subsection}{\numberline {1.8.6}Kil\IeC {\'e}p\IeC {\'e}si duzzad\IeC {\'a}s}{20}{subsection.1.8.6}
\contentsline {subsection}{\numberline {1.8.7}Folyad\IeC {\'e}k lamin\IeC {\'a}ris \IeC {\'a}raml\IeC {\'a}sa kapill\IeC {\'a}risban}{20}{subsection.1.8.7}
\contentsline {section}{\numberline {1.9}Kilencedik el\IeC {\H o}ad\IeC {\'a}s}{21}{section.1.9}
\contentsline {subsection}{\numberline {1.9.1}El\IeC {\H o}k\IeC {\'e}sz\IeC {\'\i }t\IeC {\'e}s}{21}{subsection.1.9.1}
\contentsline {subsubsection}{\numberline {1.9.1.1}Kever\IeC {\'e}s}{21}{subsubsection.1.9.1.1}
\contentsline {subsubsection}{\numberline {1.9.1.2}Sz\IeC {\'a}r\IeC {\'\i }t\IeC {\'a}s}{22}{subsubsection.1.9.1.2}
\contentsline {section}{\numberline {1.10}Tizedik el\IeC {\H o}ad\IeC {\'a}s}{23}{section.1.10}
\contentsline {subsection}{\numberline {1.10.1}Szakaszos elj\IeC {\'a}r\IeC {\'a}sok}{23}{subsection.1.10.1}
\contentsline {subsection}{\numberline {1.10.2}Folytonos elj\IeC {\'a}r\IeC {\'a}sok}{24}{subsection.1.10.2}
\contentsline {section}{\numberline {1.11}Tizenegyedik el\IeC {\H o}ad\IeC {\'a}s}{24}{section.1.11}
\contentsline {subsection}{\numberline {1.11.1}Kalanderez\IeC {\'e}s}{25}{subsection.1.11.1}
\contentsline {subsubsection}{\numberline {1.11.1.1}Pr\IeC {\'e}gel\IeC {\'e}s/bark\IeC {\'a}z\IeC {\'a}s}{25}{subsubsection.1.11.1.1}
\contentsline {subsubsection}{\numberline {1.11.1.2}Lamin\IeC {\'a}l\IeC {\'a}s}{25}{subsubsection.1.11.1.2}
\contentsline {subsection}{\numberline {1.11.2}Melegalak\IeC {\'\i }t\IeC {\'a}s}{26}{subsection.1.11.2}
\contentsline {subsection}{\numberline {1.11.3}\IeC {\"U}reges testek gy\IeC {\'a}rt\IeC {\'a}sa}{27}{subsection.1.11.3}
\contentsline {subsubsection}{\numberline {1.11.3.1}Extr\IeC {\'u}zi\IeC {\'o}s f\IeC {\'u}v\IeC {\'a}s}{27}{subsubsection.1.11.3.1}
\contentsline {section}{\numberline {1.12}Tizenkettedik el\IeC {\H o}ad\IeC {\'a}s}{27}{section.1.12}
\contentsline {subsection}{\numberline {1.12.1}Fr\IeC {\"o}ccsf\IeC {\'u}v\IeC {\'a}s}{27}{subsection.1.12.1}
\contentsline {subsection}{\numberline {1.12.2}Rot\IeC {\'a}ci\IeC {\'o}s \IeC {\"o}nt\IeC {\'e}s}{28}{subsection.1.12.2}
\contentsline {subsection}{\numberline {1.12.3}Dekor\IeC {\'a}ci\IeC {\'o}s technol\IeC {\'o}gi\IeC {\'a}k}{28}{subsection.1.12.3}
\contentsline {chapter}{\numberline {2}Szillabuszok}{30}{chapter.2}
\contentsline {section}{\numberline {2.0}Anyagismereti alapok}{30}{section.2.0}
\contentsline {subsection}{\numberline {2.0.1}Alapfogalmak}{30}{subsection.2.0.1}
\contentsline {subsection}{\numberline {2.0.2}Csoportos\IeC {\'\i }t\IeC {\'a}s}{30}{subsection.2.0.2}
\contentsline {subsection}{\numberline {2.0.3}Anyagszerkezet}{31}{subsection.2.0.3}
\contentsline {subsection}{\numberline {2.0.4}T\IeC {\"o}megm\IeC {\H u}anyagok}{31}{subsection.2.0.4}
\contentsline {subsubsection}{\numberline {2.0.4.1}Polietil\IeC {\'e}n - PE}{31}{subsubsection.2.0.4.1}
\contentsline {subsubsection}{\numberline {2.0.4.2}Polipropil\IeC {\'e}n - PP}{32}{subsubsection.2.0.4.2}
\contentsline {subsubsection}{\numberline {2.0.4.3}Polivinil-klorid - PVC}{32}{subsubsection.2.0.4.3}
\contentsline {subsubsection}{\numberline {2.0.4.4}Polietil\IeC {\'e}n-tereftal\IeC {\'a}t - PET}{32}{subsubsection.2.0.4.4}
\contentsline {subsubsection}{\numberline {2.0.4.5}Polisztirol - PS}{32}{subsubsection.2.0.4.5}
\contentsline {subsection}{\numberline {2.0.5}M\IeC {\H u}szaki m\IeC {\H u}anyagok}{32}{subsection.2.0.5}
\contentsline {subsubsection}{\numberline {2.0.5.1}Poliuret\IeC {\'a}n - PUR}{33}{subsubsection.2.0.5.1}
\contentsline {subsubsection}{\numberline {2.0.5.2}Akrilnitril-butadi\IeC {\'e}n-sztirol - ABS}{33}{subsubsection.2.0.5.2}
\contentsline {subsubsection}{\numberline {2.0.5.3}Poliamid - PA}{33}{subsubsection.2.0.5.3}
\contentsline {subsubsection}{\numberline {2.0.5.4}Polikarbon\IeC {\'a}t - PC}{33}{subsubsection.2.0.5.4}
\contentsline {subsubsection}{\numberline {2.0.5.5}Polioximetil\IeC {\'e}n - POM}{33}{subsubsection.2.0.5.5}
\contentsline {subsubsection}{\numberline {2.0.5.6}Polimetil-metilakril\IeC {\'a}t - PMMA}{34}{subsubsection.2.0.5.6}
\contentsline {section}{\numberline {2.1}Szak\IeC {\'\i }t\IeC {\'a}s-k\IeC {\'u}sz\IeC {\'a}s (M1)}{34}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Szak\IeC {\'\i }t\IeC {\'o}diagram - mechanikai jellemz\IeC {\H o}k}{34}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Viszkoelaszticit\IeC {\'a}s}{34}{subsection.2.1.2}
\contentsline {subsubsection}{\numberline {2.1.2.1}K\IeC {\'u}sz\IeC {\'a}svizsg\IeC {\'a}lat}{36}{subsubsection.2.1.2.1}
\contentsline {section}{\numberline {2.2}Az extr\IeC {\'u}zi\IeC {\'o} alapjai (M2)}{36}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Extrudszersz\IeC {\'a}mok}{38}{subsection.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.1.1}Lemezgy\IeC {\'a}rt\IeC {\'a}s}{38}{subsubsection.2.2.1.1}
\contentsline {subsubsection}{\numberline {2.2.1.2}F\IeC {\'o}liaf\IeC {\'u}v\IeC {\'a}s}{38}{subsubsection.2.2.1.2}
\contentsline {subsubsection}{\numberline {2.2.1.3}Cs\IeC {\H o}gy\IeC {\'a}rt\IeC {\'a}s}{38}{subsubsection.2.2.1.3}
\contentsline {subsubsection}{\numberline {2.2.1.4}Profilszersz\IeC {\'a}mok}{39}{subsubsection.2.2.1.4}
\contentsline {section}{\numberline {2.3}Fr\IeC {\"o}ccs\IeC {\"o}nt\IeC {\'e}s (M3)}{39}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}A fr\IeC {\"o}ccs\IeC {\"o}nt\IeC {\H o}g\IeC {\'e}p}{39}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Fr\IeC {\"o}ccs\IeC {\"o}nt\IeC {\'e}si ciklus}{40}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}\IeC {\"O}mled\IeC {\'e}k \IeC {\'a}llapothat\IeC {\'a}roz\IeC {\'o}i}{41}{subsection.2.3.3}
\contentsline {section}{\numberline {2.4}3D nyomtat\IeC {\'a}s (M4)}{41}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}STL form\IeC {\'a}tum}{42}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}Addit\IeC {\'\i }v gy\IeC {\'a}rt\IeC {\'a}stechnol\IeC {\'o}gi\IeC {\'a}k (AM)}{42}{subsection.2.4.2}
\contentsline {subsubsection}{\numberline {2.4.2.1}3D printing}{43}{subsubsection.2.4.2.1}
\contentsline {subsubsection}{\numberline {2.4.2.2}FDM - \IeC {\"o}mled\IeC {\'e}kr\IeC {\'e}tegez\IeC {\'e}s}{43}{subsubsection.2.4.2.2}
\contentsline {subsubsection}{\numberline {2.4.2.3}Fotopolimer}{43}{subsubsection.2.4.2.3}
\contentsline {section}{\numberline {2.5}Kompozitok (M5)}{44}{section.2.5}
\contentsline {subsection}{\numberline {2.5.1}Er\IeC {\H o}s\IeC {\'\i }t\IeC {\H o}k}{44}{subsection.2.5.1}
\contentsline {subsubsection}{\numberline {2.5.1.1}Paradoxonok}{45}{subsubsection.2.5.1.1}
\contentsline {subsubsection}{\numberline {2.5.1.2}Anyagai}{45}{subsubsection.2.5.1.2}
\contentsline {subsection}{\numberline {2.5.2}M\IeC {\'a}trixok}{46}{subsection.2.5.2}
\contentsline {subsection}{\numberline {2.5.3}Gy\IeC {\'a}rt\IeC {\'a}s}{47}{subsection.2.5.3}
\contentsline {subsection}{\numberline {2.5.4}Szendvicsszerkezet}{48}{subsection.2.5.4}
